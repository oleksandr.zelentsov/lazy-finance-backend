from importlib import import_module

from fastapi import FastAPI

from api.middleware import MyCORSMiddleware

ORIGINS = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:8000",
    "http://localhost:3000",
]

app = FastAPI()
api_router_modules = [
    "api.auth.views",
    "api.budget.views",
    "api.charts",
]
app.add_middleware(
    MyCORSMiddleware,
    allow_origins=ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

for module_name in api_router_modules:
    try:
        module = import_module(module_name)
        router = module.router
    except (ImportError, AttributeError) as e:
        print(e)
        continue

    app.include_router(router)
