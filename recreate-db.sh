#!/bin/sh

docker compose exec backend python -c "from db.database import create_tables, drop_tables; drop_tables(); create_tables()"
