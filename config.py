from functools import lru_cache

from pydantic import BaseSettings


class Settings(BaseSettings):
    db_host: str = ""
    postgres_password: str = ""
    postgres_user: str = ""
    postgres_db: str = ""
    database_url: str = ""
    jwt_key: str
    jwt_access_token_max_age_seconds: int
    jwt_refresh_token_max_age_seconds: int

    class Config:
        env_file = ".env"


@lru_cache()
def get_settings():
    return Settings()
