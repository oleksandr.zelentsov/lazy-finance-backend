from datetime import datetime, timedelta, timezone
from secrets import choice
from string import ascii_letters, ascii_lowercase, ascii_uppercase

import jwt
from bcrypt import gensalt, hashpw

ALPHABET = ascii_letters + ascii_lowercase + ascii_uppercase


def random_string(size: int):
    return "".join(choice(ALPHABET) for _ in range(size))


def get_hashed_password(password: str, salt=None):
    salt = salt or gensalt().decode()
    return hashpw(password.encode(), salt.encode()).decode(), salt


def get_jwt_token(username, age: timedelta, key: str):
    return jwt.encode(
        {
            "exp": datetime.now(timezone.utc) + age,
            "username": username,
        },
        key=key,
        algorithm="HS256",
    )


def decode_jwt_token(token: str, key: str):
    return jwt.decode(
        token,
        key=key,
        algorithms="HS256",
        options={
            "require": ["exp", "username"],
        },
    )
