FROM python:3.10

RUN mkdir -p /app
WORKDIR /app
COPY .env /app/
COPY *.py /app/
COPY api /app/
COPY db /app/
COPY run-server.sh /app/
COPY requirements* /app/
RUN pip install -r /app/requirements.txt
EXPOSE 8000

CMD ["./run-server.sh"]
