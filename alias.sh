#!/bin/bash

alias dc="docker compose"
alias dceb="dc exec backend"
alias dcesh="dc exec backend python"
alias dl="dc logs -f"
alias dls="dl --since=1m"
alias dcdo="dc down"
alias run="dc build && dc up -d"
