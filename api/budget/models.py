from datetime import datetime

from fastapi import HTTPException
from lazy_budget.budget import Budget as LbudBudget
from lazy_budget.budget import FinancialOperation as LbudOperation
from lazy_budget.money_provider import Currency, Money
from peewee import (
    BooleanField,
    CharField,
    DateField,
    DateTimeField,
    DecimalField,
    ForeignKeyField,
)

from api.auth.models import User
from db.base import BaseModel


class CurrencyField(CharField):
    def __init__(self):
        super().__init__(choices=[(c.name, c.value) for c in Currency])


class Category(BaseModel):
    name = CharField()
    user = ForeignKeyField(User, backref="operation_categories")


class Operation(BaseModel):
    amount = DecimalField(decimal_places=2, auto_round=True)
    currency = CurrencyField()
    category = ForeignKeyField(Category, backref="operations", null=True)
    description = CharField()
    performed_at = DateTimeField(default=datetime.now)
    user = ForeignKeyField(User, backref="operations")

    def as_lbud(self) -> LbudOperation:
        return LbudOperation(
            money_value=Money(self.amount, Currency[self.currency]),
            description=self.description or "",
            dttm=self.performed_at,
            category=(self.category.name if self.category else None),
        )


class Budget(BaseModel):
    user = ForeignKeyField(User, backref="budgets")
    total_amount = DecimalField(decimal_places=2)
    keep_amount = DecimalField(decimal_places=2)
    start = DateField()
    end = DateField()
    currency = CurrencyField()
    archived = BooleanField(default=False)

    def as_lbud(self) -> LbudBudget:
        today = datetime.now()
        return LbudBudget(
            total=Money(self.total_amount, Currency[self.currency]),
            keep=Money(self.keep_amount, Currency[self.currency]),
            start=self.start,
            end=self.end,
            currency=Currency[self.currency],
            today=today.date(),
        )

    @classmethod
    def get_current_budget(cls, user: User, raise_error=True) -> "Budget":
        now = datetime.now()
        try:
            return cls.get(
                Budget.user == user,
                Budget.start <= now,
                Budget.end >= now,
                Budget.archived == False,
            )
        except Budget.DoesNotExist as e:
            if raise_error:
                raise HTTPException(404, "No budget for current period.") from e
            else:
                return None
