import re
from datetime import date, datetime, time
from decimal import Decimal
from random import shuffle

import pandas
from faker import Faker
from lazy_budget.helpers import generate_sequence_by_average_tree

from api.budget.models import Category, Operation

faker = Faker()
description_with_category_pattern = re.compile(r"\[([\w\s_ ]+)\]( [\w\s_ ]+)?")


async def csv_file_example(n: int = 10):
    amounts = generate_sequence_by_average_tree(
        Decimal("-10"),
        n,
        start=-100,
        end=100,
    )
    will_be_category = [
        x > 0
        for x in generate_sequence_by_average_tree(
            Decimal("0"),
            n,
            start=-1,
            end=1,
        )
    ]
    shuffle(amounts)
    shuffle(will_be_category)
    now = datetime.now()
    for amount, is_category in zip(amounts, will_be_category):
        description = " ".join(faker.words(nb=5)).strip()
        datetime_ = faker.date_time(end_datetime=now)
        if is_category:
            category = ""
            while not category:
                category = faker.word()

            description = f"[{category}] {description}"

        yield f"{amount},{description},{datetime_.isoformat()}\n"


def import_row(row: pandas.Series, current_user, budget) -> tuple[Operation, bool]:
    description = row["description"] or ""
    if match := re.match(
        description_with_category_pattern,
        description.strip(),
    ):
        category_name, description = match.groups()
        description = description or ""
        description = description.strip()
    else:
        category_name, description = None, description

    if category_name:
        category, _ = Category.get_or_create(
            name=category_name,
            user=current_user,
        )
    else:
        category = None

    amount = Decimal(row["amount"])
    performed_at = row["dttm"]
    if " " in performed_at:
        date_, time_ = performed_at.split()
        performed_at = datetime.combine(
            date.fromisoformat(date_),
            time.fromisoformat(time_),
        )
    else:
        performed_at = datetime.fromisoformat(performed_at)

    op, created = Operation.get_or_create(
        user_id=current_user.id,
        amount=str(amount),
        performed_at=performed_at.isoformat(),
        currency=budget.currency,
        description=description,
        category_id=category.id if category else None,
    )

    return op, created
