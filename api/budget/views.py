import logging

from fastapi import APIRouter, Depends, HTTPException, Response, UploadFile
from lazy_budget.money_provider import Currency
from lazy_budget.stats import BasicBudgetStats
from pandas import read_csv
from starlette.responses import StreamingResponse

from api.auth.dependencies import current_user
from api.auth.models import User
from api.budget.helpers import csv_file_example, import_row
from api.budget.models import Budget, Category, Operation
from api.budget.serializers import BasicBudgetStats as BasicBudgetStatsSerializer
from api.budget.serializers import Budget as BudgetSerializer
from api.budget.serializers import Category as CategorySerializer
from api.budget.serializers import CsvImportResponse
from api.budget.serializers import Operation as OperationSerializer

router = APIRouter(prefix="/budget", tags=["budget"])


@router.get("/currencies")
def currency_list() -> list[str]:
    return [c.value for c in Currency]


@router.get("/operations")
def operations(
    category: str = "",
    current_user: User = Depends(current_user),
) -> list[OperationSerializer]:
    if category:
        try:
            category = Category.get(
                Category.user == current_user,
                Category.name == category,
            )
        except Category.DoesNotExist:
            ops = current_user.operationss
        else:
            ops = filter(lambda op: op.category == category, current_user.operations)
    else:
        ops = current_user.operations

    ops = ops.order_by(Operation.performed_at.desc())
    return list(
        map(
            OperationSerializer.from_orm,
            ops,
        )
    )


@router.post("/operations", response_model=OperationSerializer)
def operation_create(
    operation: OperationSerializer,
    current_user: User = Depends(current_user),
):
    if category_name := operation.category:
        category, _ = Category.get_or_create(
            name=category_name,
            user=current_user,
        )
    else:
        category = None

    op = Operation(
        amount=operation.amount,
        currency=operation.currency.value,
        category=category,
        description=operation.description,
        performed_at=operation.performed_at,
        user=current_user,
    )
    op.save()
    return Response(
        OperationSerializer.from_orm(op).json(),
        status_code=201,
    )


@router.put("/operations/{operation_id}", response_model=OperationSerializer)
def operation_edit(
    operation: OperationSerializer,
    operation_id: int,
    current_user: User = Depends(current_user),
):
    if category_name := operation.category:
        category, _ = Category.get_or_create(
            name=category_name,
            user=current_user,
        )
    else:
        category = None

    try:
        op = Operation.get(Operation.id == operation_id, Operation.user == current_user)
    except Operation.DoesNotExist as e:
        raise HTTPException(404, "No operation with the chosen id.") from e

    op.amount = operation.amount
    op.category = category
    op.currency = operation.currency.name
    op.description = operation.description
    op.performed_at = operation.performed_at
    op.save()
    return OperationSerializer.from_orm(op)


@router.delete("/operations/{operation_id}")
def operation_delete(
    operation_id: int,
    current_user: User = Depends(current_user),
):
    try:
        op = Operation.get(
            Operation.id == operation_id,
            Operation.user == current_user,
        )
    except Operation.DoesNotExist:
        return Response("", status_code=204)

    op.delete_instance()
    return Response("", status_code=204)


@router.delete("/operations")
def clear_operations(current_user: User = Depends(current_user)):
    Operation.delete().where(Operation.user == current_user).execute()
    return Response("", status_code=204)


@router.get("/", response_model=BudgetSerializer)
def budget(current_user: User = Depends(current_user)):
    budget = Budget.get_current_budget(current_user)
    return BudgetSerializer.from_orm(budget)


@router.put("/", response_model=BudgetSerializer)
def budget_edit(
    budget_changes: BudgetSerializer,
    current_user: User = Depends(current_user),
):
    budget = Budget.get_current_budget(current_user)
    budget.total_amount = budget_changes.total_amount
    budget.keep_amount = budget_changes.keep_amount
    budget.start = budget_changes.start
    budget.end = budget_changes.end
    budget.currency = budget_changes.currency.name
    budget.save()
    return BudgetSerializer.from_orm(budget)


@router.delete("/")
def budget_delete(current_user: User = Depends(current_user)):
    try:
        budget = Budget.get_current_budget(current_user)
    except HTTPException:
        return Response("", 204)

    budget.archived = True
    budget.save()
    return Response("", 204)


@router.post("/", response_model=BudgetSerializer)
def create_budget(
    budget: BudgetSerializer,
    current_user: User = Depends(current_user),
):
    if Budget.get_current_budget(current_user, raise_error=False):
        raise HTTPException(400, "Budget already exists for this period.")

    budget = Budget(
        user=current_user,
        total_amount=budget.total_amount,
        keep_amount=budget.keep_amount,
        start=budget.start,
        end=budget.end,
        currency=budget.currency.value,
    )
    budget.save()
    return BudgetSerializer.from_orm(budget)


@router.get("/stats", response_model=BasicBudgetStatsSerializer)
def budget_stats(current_user: User = Depends(current_user)):
    budget = Budget.get_current_budget(current_user).as_lbud()
    operations = [op.as_lbud() for op in current_user.operations]
    return BasicBudgetStatsSerializer.from_lbud(
        BasicBudgetStats.get_stats(budget, operations)
    )


@router.get("/categories")
def category_list(current_user: User = Depends(current_user)):
    return list(map(CategorySerializer.from_orm, current_user.operation_categories))


@router.delete("/categories")
def clear_categories(current_user: User = Depends(current_user)):
    Operation.update({Operation.category: None}).where(
        Operation.category.is_null(False),
        Operation.user == current_user,
    ).execute()
    Category.delete().where(Category.user == current_user).execute()
    return Response("", 201)


@router.get("/operations/csv-import-example")
async def csv_import_operations_example(n: int = 10):
    gen = csv_file_example(n)
    return StreamingResponse(
        gen,
        media_type="text/csv",
    )


@router.post("/operations/csv-import")
def csv_import_operations(
    file: UploadFile,
    current_user: User = Depends(current_user),
):
    budget = Budget.get_current_budget(current_user)
    df = read_csv(
        file.file,
        names=["amount", "description", "dttm"],
    )
    created_records = []
    for row in df.to_dict("records"):
        try:
            op, created = import_row(row, current_user, budget)
        except Exception as e:
            logging.exception(e)
            continue

        if created:
            created_records.append(op)

    return CsvImportResponse(created=len(created_records))
