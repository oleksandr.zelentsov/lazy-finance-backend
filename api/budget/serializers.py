from datetime import date, datetime
from decimal import Decimal

from lazy_budget.money_provider import Currency
from lazy_budget.stats import BasicBudgetStats as LbudBasicBudgetStats
from pydantic import BaseModel, Field


class Money(BaseModel):
    amount: Decimal
    currency: str


class Operation(BaseModel):
    amount: Decimal
    currency: Currency
    category: str = None
    description: str = ""
    performed_at: datetime = Field(default_factory=datetime.now)
    id: int = None

    @classmethod
    def from_orm(cls, obj):
        return cls(
            id=obj.id,
            amount=obj.amount,
            currency=Currency[obj.currency],
            category=obj.category.name if obj.category else None,
            description=obj.description,
            performed_at=obj.performed_at,
        )


class Budget(BaseModel):
    total_amount: Decimal
    keep_amount: Decimal
    start: date
    end: date
    currency: Currency
    id: int = None

    @classmethod
    def from_orm(cls, budget):
        return cls(
            id=budget.id,
            total_amount=budget.total_amount,
            keep_amount=budget.keep_amount,
            start=budget.start,
            end=budget.end,
            currency=budget.currency,
        )


class BasicBudgetStats(BaseModel):
    total_days: int
    days_left: int
    available_per_day: Money
    can_spend_today: Money
    avg_spent_per_day: Money
    currently_keeping: Money
    total_available: Money
    total_spent: Money
    maximum_spent: Money
    days_until_can_spend: int

    @classmethod
    def from_lbud(cls, stats: LbudBasicBudgetStats):
        return cls(
            total_days=stats.total_days,
            days_left=stats.days_left,
            available_per_day=Money(
                amount=stats.available_per_day.amount,
                currency=stats.available_per_day.currency.name,
            ),
            can_spend_today=Money(
                amount=stats.can_spend_today.amount,
                currency=stats.can_spend_today.currency.name,
            ),
            avg_spent_per_day=Money(
                amount=stats.avg_spent_per_day.amount,
                currency=stats.avg_spent_per_day.currency.name,
            ),
            currently_keeping=Money(
                amount=stats.currently_keeping.amount,
                currency=stats.currently_keeping.currency.name,
            ),
            total_available=Money(
                amount=stats.total_available.amount,
                currency=stats.total_available.currency.name,
            ),
            total_spent=Money(
                amount=stats.total_spent.amount,
                currency=stats.total_spent.currency.name,
            ),
            maximum_spent=Money(
                amount=stats.maximum_spent.amount,
                currency=stats.maximum_spent.currency.name,
            ),
            days_until_can_spend=stats.days_until_can_spend,
        )


class Category(BaseModel):
    id: int
    name: str

    @classmethod
    def from_orm(cls, obj):
        return cls(id=obj.id, name=obj.name)


class CsvImportResponse(BaseModel):
    created: int
