from fastapi import APIRouter, Depends, HTTPException

from api.auth.dependencies import current_user
from api.auth.models import Token, User
from api.auth.serializers import LoginData, RefreshToken, UserData

router = APIRouter(prefix="/auth", tags=["auth"])


@router.post("/register")
def register(user_data: UserData):
    if User.select().where(User.username == user_data.username).exists():
        raise HTTPException(400, "Cannot use this username.")

    user = User.build_user(
        username=user_data.username,
        password=user_data.password,
    )
    user.save()
    return "OK"


@router.post("/login")
def login(user_data: LoginData):
    if user := User.authenticate(user_data.username, user_data.password):
        token = Token.generate_token(
            user,
            access_token_age=user_data.access_token_age,
            refresh_token_age=user_data.refresh_token_age,
        )
        token.save()
        return {
            "access_token": token.access_token,
            "refresh_token": token.refresh_token,
        }
    else:
        raise HTTPException(401, "Invalid username or password.")


@router.post("/refresh-token")
def refresh_token(token_data: RefreshToken):
    token_str = token_data.refresh_token
    try:
        token: Token = Token.get(Token.refresh_token == token_str)
    except Token.DoesNotExist as e:
        raise HTTPException(400, "Invalid refresh token.") from e

    if not token.is_refresh_valid:
        token.delete_instance()
        raise HTTPException(400, "Invalid refresh token.")

    new_token = Token.generate_token(token.user)
    token.delete_instance()
    new_token.save()
    return {
        "access_token": new_token.access_token,
        "refresh_token": new_token.refresh_token,
    }


@router.get("/check")
def check_auth(user: User = Depends(current_user)):
    if user:
        return {"username": user.username}
    else:
        raise HTTPException(401, "You are not authenticated.")
