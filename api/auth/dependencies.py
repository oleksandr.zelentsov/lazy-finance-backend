from fastapi import Header, HTTPException

from api.auth.models import Token, User


def current_user(authorization: str | None = Header(default=None)) -> User:
    if not authorization or not authorization.startswith("Bearer "):
        raise HTTPException(401, "You need to be logged in to use this resource.")

    _, token_str = authorization.split()
    try:
        token = Token.get(Token.access_token == token_str)
    except Token.DoesNotExist as e:
        raise HTTPException(401, "Invalid token.") from e

    if token.is_access_valid:
        return token.user
    else:
        raise HTTPException(401, "Invalid token.")
