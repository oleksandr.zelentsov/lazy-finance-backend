import inspect
from functools import wraps

from fastapi import Depends

from api.auth.dependencies import current_user
from api.auth.models import User


def login_required(func):
    @wraps(func)
    def new_func(*args, current_user: User = Depends(current_user), **kwargs):
        if "current_user" in inspect.getfullargspec(func)[0]:
            return func(*args, current_user=current_user, **kwargs)

        return func(*args, **kwargs)

    return new_func
