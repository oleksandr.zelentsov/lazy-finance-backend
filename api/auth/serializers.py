from pydantic import BaseModel, Field

from config import Settings, get_settings

settings: Settings = get_settings()


class UserData(BaseModel):
    username: str
    password: str


class LoginData(BaseModel):
    username: str
    password: str
    access_token_age: int = Field(
        description="access token age in seconds",
        gt=0,
        le=settings.jwt_access_token_max_age_seconds,
        default=settings.jwt_access_token_max_age_seconds,
    )
    refresh_token_age: int = Field(
        description="refresh token age in seconds",
        gt=0,
        le=settings.jwt_refresh_token_max_age_seconds,
        default=settings.jwt_refresh_token_max_age_seconds,
    )


class RefreshToken(BaseModel):
    refresh_token: str
