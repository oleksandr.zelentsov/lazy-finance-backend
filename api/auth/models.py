from datetime import datetime, timedelta

import jwt
from peewee import CharField, DateTimeField, ForeignKeyField

from config import get_settings
from db.base import BaseModel
from utils import decode_jwt_token, get_hashed_password, get_jwt_token

settings = get_settings()
MAX_TOKENS_ALLOWED = 5


class User(BaseModel):
    username = CharField(unique=True)
    password = CharField()
    salt = CharField()

    @classmethod
    def build_user(cls, username: str, password: str):
        hashed_password, salt = get_hashed_password(password)
        return cls(
            username=username,
            password=hashed_password,
            salt=salt,
        )

    @classmethod
    def authenticate(cls, username: str, password: str) -> "User":
        users = cls.select().where(cls.username == username)
        for user in users:
            hashed_password, _ = get_hashed_password(password, user.salt)
            if hashed_password == user.password:
                return user

    def update_password(self, new_password):
        self.password, self.salt = get_hashed_password(new_password)


class Token(BaseModel):
    user = ForeignKeyField(User, backref="tokens")
    access_token = CharField()
    refresh_token = CharField()
    created_at = DateTimeField(default=datetime.now)

    def save(self, force_insert=False, only=None):
        all_tokens = list(self.user.tokens)
        to_delete = all_tokens[MAX_TOKENS_ALLOWED + 1 :]  # noqa E203
        print("deleting", len(to_delete), "tokens")
        for token in to_delete:
            token.delete_instance()

        return super().save(force_insert, only)

    @classmethod
    def generate_token(
        cls,
        user: User,
        access_token_age: int = settings.jwt_access_token_max_age_seconds,
        refresh_token_age: int = settings.jwt_refresh_token_max_age_seconds,
    ) -> "Token":
        access_token = get_jwt_token(
            username=user.username,
            age=timedelta(seconds=access_token_age),
            key=settings.jwt_key,
        )
        refresh_token = get_jwt_token(
            username=user.username,
            age=timedelta(seconds=refresh_token_age),
            key=settings.jwt_key,
        )
        return cls(
            user=user,
            access_token=access_token,
            refresh_token=refresh_token,
        )

    @classmethod
    def is_token_valid(cls, token: str, user: User = None) -> bool:
        try:
            decoded = decode_jwt_token(token, settings.jwt_key)
        except (jwt.ExpiredSignatureError, jwt.MissingRequiredClaimError):
            return False

        return not user or decoded["username"] == user.username

    @property
    def is_access_valid(self):
        return self.is_token_valid(self.access_token, self.user)

    @property
    def is_refresh_valid(self):
        return self.is_token_valid(self.refresh_token, self.user)
