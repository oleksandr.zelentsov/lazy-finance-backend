from typing import Type

from fastapi import APIRouter, Depends, Response
from lazy_budget.charts import BaseChart, BurnDownChart
from matplotlib import use

from api.auth.dependencies import current_user
from api.auth.models import User
from api.budget.models import Budget, Operation

router = APIRouter(prefix="/charts", tags=["charts"])


def get_chart_file(chart_type: Type[BaseChart], *chart_args, **chart_kwargs):
    use("Agg")
    c = chart_type(*chart_args, **chart_kwargs)
    f = c.get_file()
    return Response(f, media_type="image/png")


@router.get("/burndown")
def burndown(
    show_zero: bool = False,
    savings: bool = False,
    show_both: bool = False,
    whole_budget: bool = False,
    current_user: User = Depends(current_user),
):
    return get_chart_file(
        BurnDownChart,
        budget=Budget.get_current_budget(current_user).as_lbud(),
        operations=list(map(Operation.as_lbud, current_user.operations)),
        show_zero=show_zero,
        savings=savings,
        show_both=show_both,
        whole_budget=whole_budget,
    )
