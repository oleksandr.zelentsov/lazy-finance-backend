from peewee import PostgresqlDatabase
from playhouse.db_url import connect

from config import get_settings

settings = get_settings()

if settings.database_url:
    database = connect(
        settings.database_url,
        autorollback=True,
    )
else:
    database = PostgresqlDatabase(
        settings.postgres_db,
        user=settings.postgres_user,
        password=settings.postgres_password,
        host=settings.db_host,
        autorollback=True,
    )


def get_tables():
    from api.auth.models import Token, User
    from api.budget.models import Budget, Category, Operation

    return [Token, User, Category, Operation, Budget]


def create_tables():
    with database:
        database.create_tables(get_tables())


def drop_tables():
    with database:
        database.drop_tables(get_tables())
