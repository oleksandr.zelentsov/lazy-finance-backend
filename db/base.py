from peewee import Model

from db.database import database


class BaseModel(Model):
    class Meta:
        database = database
